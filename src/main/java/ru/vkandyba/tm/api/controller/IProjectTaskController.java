package ru.vkandyba.tm.api.controller;

import ru.vkandyba.tm.model.Project;

public interface IProjectTaskController {

    void bindTaskToProjectById();

    void unbindTaskToProjectById();

    void findAllTaskByProjectId();

    void removeById();

}
