package ru.vkandyba.tm;

import ru.vkandyba.tm.component.Bootstrap;

public class Application {

    public static void main(String[] args) {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.start(args);
    }

}

